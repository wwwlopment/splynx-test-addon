<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'Splynx Add-on TestAddon';
    }

    public function getModuleName()
    {
        return 'splynx_addon_testaddon';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
        ];
    }

    public function getEntryPoints()
    {
        return [
            [
                'name' => 'testaddon_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Ftestaddon',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
}
