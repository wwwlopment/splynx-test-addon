<?php

$paramsFile = __DIR__ . DIRECTORY_SEPARATOR . 'params.php';
if (!file_exists($paramsFile)) {
  die('Error: ' . $paramsFile . ' not found! You must copy end edit example file!');
}

$params = require($paramsFile);

$config = [
  'id' => 'splynx-test-addon',
  'basePath' => dirname(__DIR__),
  'vendorPath' => dirname(__DIR__) . '/../splynx-addon-base/vendor',
  'bootstrap' => ['log'],
  'components' => [
    'request' => [
      'baseUrl' => '/testaddon',
      'enableCookieValidation' => false
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'splynx\models\Admin',
      'idParam' => 'splynx_admin_id',
      'loginUrl' => '/admin/login/?return=%2Ftestaddon',
      'enableAutoLogin' => false,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
        '<alias:index>' => 'site/<alias>',
        '<alias:view>' => 'site/<alias>',

      ],
    ],
    'view' => [
      'class' => 'yii\web\View',
      'defaultExtension' => 'twig',
      'renderers' => [
        'twig' => [
          'class' => 'yii\twig\ViewRenderer',
          'cachePath' => '@runtime/Twig/cache',
          // Array of twig options:
          'options' => [],
          'extensions' => [
            '\splynx\components\twig\TwigActiveForm',
            '\splynx\components\twig\TwigPjax'
          ],
          'globals' => [
            'Html' => '\yii\helpers\Html',
            'Url' => '\yii\helpers\Url',
            'GridView' => '\yii\grid\GridView',
            'DatePicker' => '\yii\jui\DatePicker',
          ],
          'uses' => [
            'yii\bootstrap'
          ],
        ],
      ],
    ],
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['components']['view']['renderers']['twig']['options']['debug'] = true;
  $config['components']['view']['renderers']['twig']['options']['auto_reload'] = true;

  $config['components']['view']['renderers']['twig']['extensions'][] = '\Twig_Extension_Debug';
}

return $config;