Splynx Add-on TestAddon
======================

Splynx Add-on Skeleton based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.4.2"
composer install
~~~

Install Splynx Add-on TestAddon:
~~~
cd /var/www/splynx/addons/
git clone https://wwwlopment@bitbucket.org/wwwlopment/splynx-test-addon.git
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-test-addon/web/ /var/www/splynx/web/testaddon
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-testaddon.addons
~~~

with following content:
~~~
location /testaddon
{
        try_files $uri $uri/ /testaddon/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Some settings avaiable in `/var/www/splynx/addons/splynx-test-addon/config/params.php`.

You can then access Splynx Test Addon in menu "Customers".
=======
# splynx-test-addon

