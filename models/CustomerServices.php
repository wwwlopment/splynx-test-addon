<?php
/**
 * Created by PhpStorm.
 * User: osboxes
 * Date: 05.05.18
 * Time: 17:00
 */

namespace app\models;
use splynx\helpers\ApiHelper;


class CustomerServices extends \splynx\models\Customer {

 public static function getInternetServices()
 {
     $res = ApiHelper::getInstance()->search('admin/customers/customer/0/internet-services',
        ['customer_id' => $_GET['id']]);


     if ($res['result'] == false or empty($res['response'])) {
         return [];
     }

     $models = [];
     foreach ($res['response'] as $row) {
         $model = new static();
         static::populate($model, $row);
         $models[] = $model;
     }

     return $models;
 }

    public static function getVoiceServices()
    {
        $res = ApiHelper::getInstance()->search('admin/customers/customer/0/voice-services',
            ['customer_id' => $_GET['id']]);


        if ($res['result'] == false or empty($res['response'])) {
            return [];
        }

        $models = [];
        foreach ($res['response'] as $row) {
            $model = new static();
            static::populate($model, $row);
            $models[] = $model;
        }

        return $models;
    }

    public static function getCustomServices()
    {
        $res = ApiHelper::getInstance()->search('admin/customers/customer/0/custom-services',
            ['customer_id' => $_GET['id']]);


        if ($res['result'] == false or empty($res['response'])) {
            return [];
        }

        $models = [];
        foreach ($res['response'] as $row) {
            $model = new static();
            static::populate($model, $row);
            $models[] = $model;
        }

        return $models;
    }

}